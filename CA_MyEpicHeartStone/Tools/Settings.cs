﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_MyEpicHeartStone.Tools
{
    static class Settings
    {
        private static Dictionary<string, string> pairs = new Dictionary<string, string>();

        public static void LoadFromFile()
        {
            StreamReader reader = new StreamReader("settings.ini");

            while (reader.EndOfStream == false)
            {
                string currentString = reader.ReadLine();

                string[] tmp = currentString.Split('=');

                string key = tmp[0];
                string value = tmp[1];

                pairs.Add(key, value);
            }

            reader.Close();
        }

        public static string GetStringByKey(string key)
        {
            return pairs[key];
        }

        public static int GetIntByKey(string key)
        {
            return Convert.ToInt32(pairs[key]);
        }
    }
}
