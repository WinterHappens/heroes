﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CA_MyEpicHeartStone.Tools;

namespace CA_MyEpicHeartStone.Entities
{
    class KnightCard:Card
    {
        public KnightCard() : base(
            Settings.GetIntByKey("KNIGHT_COST"),
            Settings.GetStringByKey("KNIGHT_TYPENAME"),
            (Card.NatureType)Enum.Parse(typeof(Card.NatureType), Settings.GetStringByKey("KNIGHT_NATURE")),
            Helper.Random.Next(Settings.GetIntByKey("KNIGHT_MIN_HP"), Settings.GetIntByKey("KNIGHT_MAX_HP") + 1),
            Helper.Random.Next(Settings.GetIntByKey("KNIGHT_MIN_DAMAGE"), Settings.GetIntByKey("KNIGHT_MAX_DAMAGE") + 1))
        {
        }
    }
}
