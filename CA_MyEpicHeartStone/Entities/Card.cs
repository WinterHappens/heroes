﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CA_MyEpicHeartStone.Entities
{
    class Card
    {
        public enum NatureType
        {
            Air,
            Water,
            Ground
        }

        private int cost;
        private string typeName;
        private NatureType nature;
        private int hp;
        private int damage;

        public Card(int cost, string typeName, NatureType nature, int hp, int damage)
        {
            this.cost = cost;
            this.typeName = typeName;
            this.nature = nature;
            this.hp = hp;
            this.damage = damage;
        }




    }
}
