﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CA_MyEpicHeartStone.Tools;

namespace CA_MyEpicHeartStone.Entities
{
    class WizardCard:Card
    {
        public WizardCard() : base(
            Settings.GetIntByKey("WIZARD_COST"),
            Settings.GetStringByKey("WIZARD_TYPENAME"),
            (Card.NatureType)Enum.Parse(typeof(Card.NatureType), Settings.GetStringByKey("WIZARD_NATURE")),
            Helper.Random.Next(Settings.GetIntByKey("WIZARD_MIN_HP"), Settings.GetIntByKey("WIZARD_MAX_HP") + 1),
            Helper.Random.Next(Settings.GetIntByKey("WIZARD_MIN_DAMAGE"), Settings.GetIntByKey("WIZARD_MAX_DAMAGE") + 1))
        {
        }
    }
}
