﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CA_MyEpicHeartStone.Tools;

namespace CA_MyEpicHeartStone.Entities
{
    class DragonCard:Card
    {
        public DragonCard() : base(
            Settings.GetIntByKey("DRAGON_COST"), 
            Settings.GetStringByKey("DRAGON_TYPENAME"), 
            (Card.NatureType)Enum.Parse(typeof(Card.NatureType),Settings.GetStringByKey("DRAGON_NATURE")), 
            Helper.Random.Next(Settings.GetIntByKey("DRAGON_MIN_HP"), Settings.GetIntByKey("DRAGON_MAX_HP") + 1),
            Helper.Random.Next(Settings.GetIntByKey("DRAGON_MIN_DAMAGE"), Settings.GetIntByKey("DRAGON_MAX_DAMAGE") + 1))
        {

        }
    }
}
